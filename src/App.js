import React, { Component } from 'react';
import './App.css';
import Login from './Components/Login/Login';
import SignUp from './Components/SignUp/SignUp'

class App extends Component {
  render() {
    let view = null;
    let login = <Login/>;
    let signUp = <SignUp/>;

    view = login;
    return (
      <div>
          {view}
      </div>
    );
  }
}

export default App;
